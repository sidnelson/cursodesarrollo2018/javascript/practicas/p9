var variables={};
/* Load e instancias*/
window.addEventListener("load",()=>{
	variables.calculadora=new Calculadora(formulario.display,formulario.operaciones);
	variables.asociarNumeros=new Asociar(
									document.getElementsByClassName("numero"),
									"click",
									"variables.calculadora.numeros"
									);
	variables.asociarOperaciones=new Asociar(
									document.getElementsByClassName("operacion"),
									"click",
									"variables.calculadora.operaciones"
									);
});
/* Clase Asociar */
var Asociar=function(objeto,evento,funcion){
	/* Miembros publicos */
	this.objeto=objeto;
	this.evento=evento;
	this.funcion=funcion + "(this)";
	this.colocar=function(){
		for(var i=0;i<this.objeto.length;i++){
			var temporal=new Function(this.funcion);
			this.objeto[i].addEventListener(this.evento,temporal);
	}
	};
	this.colocar();
}
/* Clase calculadora */
var Calculadora=function(display,operaciones){
	/* Miembros privados */
	var acumulador=0;
	var acumuladorOperacion='';
	var limpia=true;
	/* Miembros publicos */
	this.operaciones=function(objeto){
		if(!limpia){ 
			if(acumulador==0){
				acumulador=parseInt(display.value);
				operaciones.value=display.value;
			}else{
				switch(acumuladorOperacion){
					case '+':
						acumulador=acumulador + parseInt(display.value);
						break;
					case '-':
						acumulador=acumulador - parseInt(display.value);
						break;
					case '*':
						acumulador=acumulador * parseInt(display.value);
						break;
					case '/':
						acumulador=acumulador / parseInt(display.value);
						break;
					default:
						acumulador=parseInt(display.value);
						operaciones.value="";
						acumuladorOperacion="";	
				};
				operaciones.value=operaciones.value + acumuladorOperacion + display.value;
				display.value=acumulador;
			}
		}
		limpia=true;
		acumuladorOperacion=objeto.value;
	};
	
	this.numeros=function(objeto){
		if(limpia){
			display.value=objeto.value;
			limpia=false;
		}else{
			display.value=display.value+objeto.value;
		}
	};
};